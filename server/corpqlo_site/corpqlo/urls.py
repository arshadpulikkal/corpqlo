from django.contrib import admin
from django.conf.urls import include, url


from . import views

urlpatterns = [
#Authentication and other user account related views
#API Views
url(r'api/home/signup',	views.SignUp,	name = 'SignUp'),
url(r'api/home/customers',	views.SignUp,	name = 'SignUp'),
url(r'api/home/login/',	views.Login,	name = 'Login'),
url(r'api/home/logout/',	views.Logout,	name = 'Logout'),
url(r'api/home/product/$', views.Products, name = 'Products'),
url(r'api/home/address/$', views.Address, name = 'Address'),
url(r'api/home/order/$', views.Orders, name='Orders'),
url(r'api/home/stock/$', views.Stock, name='Stock'),
url(r'api/home/notifyme/$', views.NotifyMe, name='NotifyMe'),
#url(r'api/changepassword/$', views.ChangePassword, name='ChangePassword'),
url(r'api/home/keyaccounts/$', views.Upload, name='Upload'),
url(r'api/home/resetpassword/$', views.ChangePassword, name='ChangePassword'),
url(r'api/home/forgotpassword/$', views.ForgotPassword, name='ForgotPassword'),
url(r'api/home/premium/$', views.Premium, name='Premium'),
url(r'api/home/subscrption/$', views.Premium, name='Premium'),
url(r'api/home/contacts/$', views.Contacts, name='Contacts'),
url(r'api/home/adminlogin/$', views.AdminLogin, name='AdminLogin'),
url(r'api/home/packages/$', views.Packages, name='Packages'),
url(r'api/home/report/$', views.Report, name='Report'),
url(r'api/home/measurment/$', views.Measurment, name='Measurment'),


]
