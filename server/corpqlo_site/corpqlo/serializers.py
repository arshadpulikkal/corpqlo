from rest_framework_mongoengine import serializers
from models import AddressModel, ProductModel, OrderModel,UserModel,MembershipModel,StockModel

class ProductSerializer(serializers.DocumentSerializer):
    class Meta:
        model = ProductModel
        fields = '__all__'

class OrderSerializer(serializers.DocumentSerializer):
    class Meta:
        model = OrderModel
        fields = '__all__'

class AddressSerializer(serializers.DocumentSerializer):
    class Meta:
        model = AddressModel
        fields = '__all__'
class UserSerializer(serializers.DocumentSerializer):
    class Meta:
        model = UserModel
        fields = '__all__'
class MembershipSerializer(serializers.DocumentSerializer):
    class Meta:
        model = MembershipModel
        fields = '__all__'
class KeyAccountSerializer(serializers.DocumentSerializer):
    class Meta:
        model = MembershipModel
        fields = '__all__'
#class StockSerializer(serializers.DocumentSerializer):
#    class Meta:
#        model = StockModel
#        fields = '__all__'

#
#
#class KeyAccountsSerializer(serializers.DocumentSerializer):
#    class Meta:
#        model = KeyAccounts
#        fields = '__all__'

