import os
import ast
import json
import sys
import pandas as pd
from datetime import datetime, date,timedelta
import random
from bson.objectid import ObjectId
#django packages
from django.shortcuts import render,render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required, user_passes_test
from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.models import User,Group
#from . import forms
from django.contrib.auth.models import Permission
from django.contrib.auth.models import Group
from django.contrib.sessions.models import Session
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.conf import settings
from django.core.mail import EmailMessage
from django.core.mail import send_mail
#rest_framework
from rest_framework import *
from rest_framework import status, viewsets
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from django.utils.dateparse import parse_datetime

from corpQlo.dataconnector import mongoConnector
from models import AddressModel, ProductModel, OrderModel,UserModel,MembershipModel,StockModel
from serializers import AddressSerializer,ProductSerializer,OrderSerializer,UserSerializer,MembershipSerializer,KeyAccountSerializer
sys.path.insert(0,"../../")
from data_service.logic import Logic
from corpqlo_site import settings
from dateutil.parser import parse
import base64
from rest_framework.authtoken.models import Token
lg = Logic()

mg = mongoConnector.MongoConnector.getInstance()
dbc = mg.getDatabaseClient('corpqlo')
staticServer = settings.STATICSERVER
#from bson.objectid import ObjectId
def handle_uploaded_file(f,Dir):
	destination_1 = os.getcwd() +"/corpqlo/static/"+Dir + "/" + str(f)
	print "f===================\n\n\n",f, "inventory"  ,Dir
	with open(os.getcwd() +"/corpqlo/static/"+Dir +"/"+ str(f), 'w+') as destination:
		print "inside  f===================\n\n\n",f 
        	for chunk in f.chunks():
           		 destination.write(chunk)
def CommunicationConnector(rule,params,typeList):
	rDict = dict()
	rDict["status"] = "success"
	try:       
		params = params.copy()
		_r = {}
		if rule == "notifyme":
			_r["messageList"] = [{"subject":params["subject"] ,"mList":[params["mList"]] , "message":params["message"]}]
		else:
			_r["messageList"] = [{"subject":params["subject"] ,"mList":[params["mList"]] , "message":params["message"]}]
		if _r["messageList"]:   
			for i in _r["messageList"]:
				try:
					send_mail(  
                                             i["subject"],
                                            i["message"],
                                             settings.EMAIL_HOST_USER,
                                             i["mList"],
                                              fail_silently=False,
                     				)	
					print ("successfully mailed ----", i )
                     			print  (i["mList"],"=====================\n\n\n\n",rDict)
                  		except Exception as e:     
                     			rDict = dict()         
                      			rDict["ERROR"] = str(e)
                     			rDict["status"] = "Error"
                     			print (rDict)
	except Exception as e:
		rDict["ERROR"] = "Error in Get message ------" + str(e)
		rDict["status"] = "Error"
		print (rDict)
	return rDict
			
def GetData(collectionName, numOfRecord, pageNumber, query={}, filters={}):
	rDict = dict()
	rDict["status"] = "Error"

	#if(query != {}):
	#	pass
	#else:
	#	rDict['status'] = 'Success'
	#	rDict['message'] = 'No parameter provided'
	#	return rDict

	# connect db
	print collectionName
	try:
		mg = mongoConnector.MongoConnector.getInstance()
		dbc = mg.getDatabaseClient('corpqlo')
		col = mg.getCollection(collectionName)
		if(filters != {}):
			doc = col.find(query, filters).limit(numOfRecord*pageNumber)
		else:
			doc = col.find(query).limit(numOfRecord*pageNumber)
	except Exception as e:
		rDict["ERROR"] = str(e)
		rDict["status"] = "Error"
		return rDict

	allData = []
	for i in doc:
		i['_id'] = str(i['_id'])
		allData.append(i)

	# pagination
	paginator = Paginator(allData, numOfRecord)
	try:
		page = paginator.page(pageNumber)
		data = page.object_list
	# If page is not an integer, deliver first page.
	except PageNotAnInteger:
		page = paginator.page(1)
		data = page.object_list
	# If page is out of range (e.g. 9999), deliver last page of results.
	except EmptyPage:
		page = paginator.page(paginator.num_pages)
		data = page.object_list

	rDict[collectionName] = data
	rDict["status"] = "Success"

	return rDict

def PostData(collectionName, serializer, record, query={}):
	rDict = dict()
	rDict["status"] = "Error"
	# check datatype
	serializer = serializer(data=record)
	if serializer.is_valid():
		print "============\n\n\nvalid",record,type(record)
		# connect db
		try:
			mg = mongoConnector.MongoConnector.getInstance()
			dbc = mg.getDatabaseClient('corpqlo')

			# check if data already exist
			if(query!={}):
				doc = mg.getCollection(collectionName).find(query).limit(1)
				docList = [i for i in doc]
				if docList:
					rDict["status"] = "Error"
					rDict["ERROR"] = "Data already exists"
					return rDict
		except Exception as e:
			rDict["ERROR:"] = str(e)
			rDict["status"] = "Error"
			return rDict

		# insert data
		try:
			record["createdAt"] = datetime.today()
			record["datetime"] = datetime.today()
			print "before inserting ====\n\n\n",record
			rDict["id"] = str(mg.insertCollection(collectionName, record))
			rDict["status"] = "Success"
			print "data is inserted"
			return rDict
		except:
			rDict["ERROR"] = "Database insert error"
			rDict["status"] = "Error"
			return rDict
	else:
		print record,serializer.errors
		rDict["ERROR"] = serializer.errors
		rDict["status"] = "Error"
		return rDict

	return rDict

def PutData(collectionName, serializer, record, query={}):
	rDict = dict()
	rDict["status"] = "Error"
	# check datatype
	try:
		# connect db
		mg = mongoConnector.MongoConnector.getInstance()
		dbc = mg.getDatabaseClient('corpqlo')

		# check if data already exist
		if "email" in query.keys():
			del query["email"]
		doc = mg.getCollection(collectionName).find(query).limit(1)
		docList = [i for i in doc]
		record["datetime"] = datetime.today()
		# if data exist
		print("record is: " +str(record) + str(type(record)) + str(collectionName) + str(query))
		if docList:
			try:
				if("_id" in record and "email" in record):
					# update data
					_id = record["_id"]
					email = record["email"]
					del record["_id"]
					del record["email"]
					_q = {}
					_q["_id"] = ObjectId(_id)
					#if email != 'admin':
					#	_q["email"] = email
					print (_q)	
					mg.getCollection(collectionName).update(
							_q,
						{
							"$set": record
						},
							upsert= False
					)
					rDict["status"] = "Success"
					#rDict["data"] = record
					print "rDict================\n\n\n",rDict
					print rDict
					return rDict
			except Exception as e:
				rDict["ERROR"] = "Error: " + str(e)
				rDict["status"] = "Error"
				print(rDict)
				return rDict
		else:
			rDict["ERROR"] = "Data does not exist"
			rDict["status"] = "Error"
			return rDict
	except Exception as e:
		rDict["ERROR:"] = str(e)
		rDict["status"] = "Error"
		return rDict

	return rDict

def DeleteData(collectionName, serializer, record, query={}):
	rDict = dict()
	rDict["status"] = "Error"
	# check datatype
	try:
		# connect db
		mg = mongoConnector.MongoConnector.getInstance()
		dbc = mg.getDatabaseClient('corpqlo')

		# check if data already exist
		if "email" in query.keys():
			if query["email"] == "admin":
				del query["email"]
	
		doc = mg.getCollection(collectionName).find(query).limit(1)
		docList = [i for i in doc]
		# if data exist
		print("record is: " +str(record) + str(type(record)))
		if docList:
			#try:
				if("_id" in record and "email" in record):
					# update data
					_id = record["_id"]
					
					email = record["email"]
					del record["_id"]
					del record["email"]
					_q = dict()
					_q["_id"] =  ObjectId(_id)
					if email  != "admin":
						_q["email"] =  email
					mg.getCollection(collectionName).update(
					 	_q,
						{
							"$set": {
								"status": "deleted"
							}
						},
						upsert= False
					)
					print "record=========\n\n\n",record
					rDict["status"] = "Success"
					#rDict["data"] = record
					print rDict
					return rDict
			#except Exception as e:
			#	rDict["ERROR"] = "Error: " + str(e)
			#	rDict["status"] = "Error"
			#	return rDict
		else:
			rDict["ERROR"] = "Data does not exist"
			rDict["status"] = "Error"
			return rDict
	except Exception as e:
		rDict["ERROR:"] = str(e)
		rDict["status"] = "Error"
		return rDict

	return rDict
@csrf_exempt
@api_view(['POST','GET'])
def AdminLogin(request):
	rJson = dict()
	rJson["status"] = "Error"
	if request.method == 'GET':
		reqDict = request.GET
		username = reqDict['username']
		password = reqDict['password']
	elif request.method == 'POST':
		reqDict = request.data
		print reqDict
		username = reqDict['username']
		password = reqDict['password']
		
	print username
	if username not in ["info@corpqlo.com"]:
		rJson["ERROR"] = "username and password are incorrect "
		rJson["status"] = "Error"
		return Response(rJson)
	try:
		u = User.objects.get(username=username)
	except:
		rJson["ERROR"] = "Username not exist"
		return Response(rJson)
	user = authenticate(username=username, password=password)
	if user is not None:
		if user.is_active:
				login(request,user)
				rJson["status"] = "Success"
				token, _ = Token.objects.get_or_create(user=user)
				rJson["token"]	= token.key

				return Response(rJson)

		else:
			rJson["status"] = "Error"
			rJson["ERROR"] = "username and password are incorrect "
			return Response(rJson)
	else:
			rJson["status"] = "Error"
			rJson["ERROR"] = "password is incorrect "
			return Response(rJson)
	return Response(rJson)


	
	
@csrf_exempt
@api_view(['POST','GET'])
def Login(request):
	rJson = dict()
	rJson["status"] = "Error"
	if request.method == 'GET':
		reqDict = request.GET
		username = reqDict['userName']
		password = reqDict['password']
	elif request.method == 'POST':
		reqDict = request.POST
		username = request.POST.get('userName')
		password = request.POST.get('password')
	try:
		u = User.objects.get(username=username)
	except:
		rJson["ERROR"] = "Username not exist"
		return Response(rJson)
	user = authenticate(username=username, password=password)
	if user is not None:
		if user.is_active:
				login(request,user)
				rJson["status"] = "Success"
				address = GetData('address',9999,1, query={"email":username}, filters= {})["address"]	
				rJson["userInfo"] = GetData('usercollection',1,50, query={"email":username}, filters= {})["usercollection"]	
				#rJson["userInfo"] = lg.GetUserInfo(username,'')["userInfo"]
				#_t,_ = Token.objects.get_or_create(user=user)
				#rJson["token"] = _t.key
				if rJson["userInfo"]:
					if "userRole" in rJson["userInfo"][0].keys():
						if rJson["userInfo"][0]["userRole"] == "admin":
							rJson["status"] = "Error"
							rJson["ERROR"] = "username and password are incorrect "
							del rJson["userInfo"]
							return Response(rJson)

					rJson["userInfo"][0]["address"] = []
					if "_id" in rJson["userInfo"][0].keys():
						rJson["userInfo"][0]["userId"] = rJson["userInfo"][0]["_id"]
						del rJson["userInfo"][0]["_id"]
					rJson["userInfo"] = rJson["userInfo"][0]
					if "date" in rJson["userInfo"].keys():
						rJson["userInfo"]["date"] = rJson["userInfo"]["date"].strftime("%s")
					if "DOB" in rJson["userInfo"].keys():
						rJson["userInfo"]["DOB"] = str(rJson["userInfo"]["DOB"])
					totalAmount = 0
					rewardPoints = 0
					if username != 'admin':
						qDict = dict()
						if "userId" in rJson["userInfo"].keys():
							qDict["userId"] = rJson["userInfo"]["userId"]
						collection = mg.getCollection('order')
						docs = collection.find(qDict)
						oList = [i for i in docs]
						if oList:
							totalAmount = 0
							rewardPoints = []
							for i in oList:
								print(i.keys(),"keys =========\n\n\n\n\n\n" , username)
								if "rewardPointsRedeemed" in i.keys():
									rewardPoints.append(int(i["rewardPointsRedeemed"]))
	

								if "status" in i.keys():
									if i["paymentMode"] in ["Cash On Delivery"] and i["status"] == "deleted":

										continue
								if "deliveryStatus" in i.keys():
									if i["deliveryStatus"]  == "delivered":
										totalAmount += i["totalAmount"]
								

					
						rJson["userInfo"]["rewardPoint"] =  round(float(totalAmount)/100)
						print ("===================================\n\n\n\n\n\n\n\n",rewardPoints)
						if rewardPoints:
							rJson["userInfo"]["rewardPoint"] =  rJson["userInfo"]["rewardPoint"]  -  sum(rewardPoints)
							print ("login ==================\n\n\n\n",rewardPoints)
						
					rMembership = GetData('membership',1,50, query={"userId":rJson["userInfo"]["userId"]}, filters= {})
					if rMembership["membership"]:
						 	rJson["userInfo"]["membershipInfo"] =  rMembership["membership"][-1].copy()

				else:
					rJson["userInfo"] = {}
				if address:
					print address,"================\n\n\n\n"
					for i in address:
						tmpDict = {}
						tmpDict = i.copy()
						#tmpDict["_id"] = i["_id"]
						if "status" in i.keys():
							continue

						rJson["userInfo"]["address"].append(tmpDict)
				token, _ = Token.objects.get_or_create(user=user)
				rJson["token"]	= token.key

				return Response(rJson)

		else:
			rJson["status"] = "Error"
			rJson["ERROR"] = "username and password are incorrect "
			return Response(rJson)
	else:
			rJson["status"] = "Error"
			rJson["ERROR"] = "password is incorrect "
			return Response(rJson)
	return Response(rJson)
@csrf_exempt
@api_view(['POST','GET','PUT'])
#@authentication_classes((SessionAuthentication, BasicAuthentication))
#@permission_classes((IsAuthenticated,))
def SignUp(request):
	rDict = dict()
	rDict["status"] = "Error"
	registerParams = dict()
	if request.method == 'GET':
		collectionName = "usercollection"
		numOfRecord = 999999
		pageNumber = 1
		query = {}
		filters = {} 
		rDict = GetData(collectionName, numOfRecord, pageNumber, query=query, filters=filters)
		if rDict["usercollection"]:
			rDict["usercollection"].sort(key=lambda x:x["date"],reverse = True)
		return Response(rDict)
	elif request.method == 'POST':
		registerParams = request.data
		print " registerParams======\n\n\n", registerParams
		if "file" in registerParams.keys():
			imgdata = base64.b64decode(registerParams["file"])
			filename = registerParams["email"]+ '.jpg'  # I assume you have a way of picking unique filenames
			with open(os.getcwd() +"/corpqlo/static/profile/" + str(filename), 'wb') as f:
    				f.write(imgdata)
			registerParams["file"] = staticServer +"profile/"+str(filename)
		#if request.FILES["file"]:
		#	_file = request.FILES["file"]
		#	handle_uploaded_file(request.FILES["file"],"profile")
			#handle_uploaded_file(_file)
		#	registerParams["file"] = "http://54.202.154.247:8000/static/profile/"+str(_file)

	elif request.method == 'PUT':
		# check parameters
		try:
			record = request.data
			if("_id" in record and "email" in record):
				pass
			else:
				raise Exception("Parameter missing")
		except Exception as e:
			rDict["ERROR:"] = "Parameters missing"
			rDict['e'] = str(e)
			return Response(rDict)
		if "file" in record.keys():
			imgdata = base64.b64decode(record["file"])
			
			filename = record["email"] + str(random.choice([1, 4, 8, 10, 3]))+ '.jpg'  # I assume you have a way of picking unique filenames
	
			with open(os.getcwd() +"/corpqlo/static/profile/" + str(filename), 'w+') as f:
				print("=======================\n\n\n\n\n\n\n",filename)
    				f.write(imgdata)
			record["file"] =  staticServer +"profile/"+str(filename)
		if  "corporate_id" in record.keys() and record["corporate_id"] != "":
				qDict = dict()
				qDict["corporate_id"] = record["corporate_id"]
				qDict["status"] = {"$nin":["deleted"]}
			 	_rDict = GetData("keyaccounts", 100, 1, query=qDict, filters={})
				if _rDict["keyaccounts"]:
					record["companyId"] =  _rDict["keyaccounts"][0]["_id"]
				else:
					rDict["status"] = "error"
					rDict["error"] = "Invalid corporate id"
					return Response(rDict)


		query =  {
				"_id": ObjectId(record["_id"]),
				"email": record["email"]
			}
		try:
			serializer = UserSerializer		
			collectionName = 'usercollection'
			rDict = PutData(collectionName, serializer, record, query=query)
			return Response(rDict)
		except Exception as e:
			rDict["status"] = "Error"
			rDict["ERROR"] = "Update error" +str(e)
			return Response(rDict)
		return Response(rDict)

	
	assert registerParams is not None ,"registerParams is not set"
	mg = mongoConnector.MongoConnector.getInstance()
	dbh = mg.getDatabaseClient(settings.COMPANY_NAME)
	registerParams["date"] = datetime.today()
	collection = mg.getCollection('usercollection')
	docs = collection.find().sort([("date",-1)]).limit(1)
	docList = [i for i in docs]
	if docList:
		if "userId" in docList[0].keys():
			registerParams['userId'] = 'CQL_'+ str(int(docList[0]["userId"][4]) +1)
		else:
			registerParams['userId'] = 'CQL_1'
	print "================\n\n,",registerParams
	if not User.objects.filter(username = registerParams["email"]).exists():
		try:

			 if  "corporate_id" in registerParams.keys() and registerParams["corporate_id"] != "":
				qDict = dict()
				qDict["corporate_id"] = registerParams["corporate_id"]
				qDict["status"] = {"$nin":["deleted"]}
			 	_rDict = GetData("keyaccounts", 100, 1, query=qDict, filters={})
				if _rDict["keyaccounts"]:
					registerParams["companyId"] =  _rDict["keyaccounts"][0]["_id"]
				else:
					rDict["status"] = "error"
					rDict["error"] = "Invalid corporate id"
					return Response(rDict)
			 u =User.objects.create_user(username =registerParams["email"],password = registerParams["password"])
			 mg.updateCollection_dash({"email":registerParams["email"]},"usercollection" ,registerParams)
			 print "successfully registered=======\n\n\n\n"
		except:
			rDict["ERROR:"] = "Please check Email....."
			return Response(rDict)
		try:
			userGroup, created = Group.objects.get_or_create(name='new_group')
		except:
			rDict["ERROR:"] = "Error in creating group....."
			return Response(rDict)
	else:

		rDict["ERROR:"] = "Email already exist"
		print rDict,"======================"
		return Response(rDict)

	print rDict,"======================"
	rDict["status"] = "success"
	return Response(rDict)

#@csrf_exempt
@api_view(['POST','GET', 'PUT','DELETE'])
def Products(request):
	rDict = dict()
	rDict["status"] = "Error"
	collectionName = "products"
	serializer = ProductSerializer
	data = []
	query = {}
	filters = {}
	try:
		# If method is GET
		if request.method == 'GET':
			'''Get data'''
			# check
			try:
				numOfRecord = request.GET['noOfRecord']
				pageNumber = request.GET['pageNumber']
			except:
				numOfRecord = 9999
				pageNumber = 1

			if request.GET.get('id'):
				query['_id'] = ObjectId(request.GET['id'])
			if request.GET.get('ids'):
				tmpList = []
				print type(request.GET['ids'])
				for i in ast.literal_eval(request.GET['ids']):
					print i
					tmpList.append(ObjectId(i))
				query['_id'] = {"$in":tmpList}
				
			if request.GET.get('category'):
				query['category'] = ObjectId(request.GET['category'])
			if request.GET.get('companyId'):
				query['companyId'] = ObjectId(request.GET['companyId'])
					
			if request.GET.get('minimalist'):
				'''filters = {
					"sku": 1,
					"name": 1,
					"company": 1,
					"size": 1,
					"price": 1,
					"stock": 1,
					"image":1,
					"description":1,
					"category":1
				}'''
			rDict = GetData(collectionName, numOfRecord, pageNumber, query=query, filters=filters)
			if rDict["products"]:
				tmpList = []
				for i in rDict["products"]:
					if i["category"] == "newArrivals":
						print (i["createdAt"] , "=============\n\n\n\n\n\n")
						if (datetime.today() - i["createdAt"]).days > 7:
							i["category"] = "CQ-Collection"
						else:
							i["category"] = "newArrivals"

					if "status"  in i.keys():
						if i["status"] == "deleted":
							continue
						stockInfo = GetData("stock", numOfRecord, pageNumber, query={"product_id":i["_id"]}, filters={})
						if stockInfo["stock"]:
							i["product_stock"] = []
							for j in stockInfo["stock"]:
								j["stockId"] = j["_id"]
								del j["_id"]
								i["product_stock"].append(j)
						tmpList.append(i)
					else:
						stockInfo = GetData("stock", numOfRecord, pageNumber, query={"product_id":i["_id"]}, filters={})
						if stockInfo["stock"]:
							i["product_stock"] = []
							for j in stockInfo["stock"]:
								if "status" in j.keys():
									if j["status"] == "deleted":
										continue

								j["stockId"] = j["_id"]
								del j["_id"]
								i["product_stock"].append(j)

						tmpList.append(i)
				rDict["products"] = []
				rDict["products"] = tmpList
						
		# If method is POST
		elif request.method == 'POST':
			'''Post data'''
			# check parameters

			try:
				record = request.data
				print "record before image upload =========\n\n",record
				#if request.method == "FILES":
				tmpDict = dict()
				if request.FILES["file"]:
                                        _file = request.FILES["file"]
					print "_file=====\n\n\n",_file,request.data,
                                        handle_uploaded_file(request.data["file"],"inventory")
					tmpDict = dict()
                                	tmpDict["file"] =  staticServer + "inventory/"+str(_file)
                                #handle_uploaded_file(_file)
                                #record = dict()
				tmpDict["file"] =  staticServer + "inventory/"+str(_file)
				tmpDict["company"] = request.POST.get("company") 
				tmpDict["sku"] = request.POST.get("sku") 
				tmpDict["name"] = request.POST.get("name")
				tmpDict["price"] = request.POST.get("price") 
				if request.POST.get("gender"):
					tmpDict["gender"] = request.POST.get("gender")
 				if request.POST.get("discount"):
					tmpDict["discount"] = request.POST.get("discount") 
				if request.POST.get("color"):
					tmpDict["color"] = request.POST.get("color") 

				if request.POST.get("description"):
					tmpDict["description"] = request.POST.get("description")
				if request.POST.get("category"):
					tmpDict["category"] = request.POST.get("category")
				if request.POST.get("fit") :
					tmpDict["fit"] = request.POST.get("fit") 
				tmpDict["image"] =  staticServer + "inventory/"+str(_file)
				tmpDict["companyId"] = request.POST.get("companyId")
				record = dict()
				record = tmpDict.copy()
				print "recordafter image upload =========\n\n",record,tmpDict

			except Exception as e:
				rDict["ERROR:"] = "Parameters missing"
				rDict['e'] = str(e)
				return Response(rDict)

			#rDict = lg.PostData(collectionName, serializer, record)
			query = {}
			try:
				rDict = PostData(collectionName, serializer, record, query=query)
				rDict["status"] = "Success"
			except Exception as e:
				rDict["status"] = "Error"
				rDict["ERROR:"] = "Post error"
				return Response(rDict)

		# if method is PUT
		elif request.method == 'PUT':
			'''Update data'''
			# check parameters
			try:
				record = request.data
				if("_id" in record and "email" in record):
					pass
				else:
					raise Exception("Parameter missing")
			except Exception as e:
				rDict["ERROR:"] = "Parameters missing"
				rDict['e'] = str(e)
				return Response(rDict)

			query =  {
				"_id": ObjectId(record["_id"]),
				#"email": record["email"]
			}
			try:
				if "file" in record.keys():
					imgdata = base64.b64decode(record["file"])
					filename = str(record["_id"])+ '.jpg'
                        		with open(os.getcwd() +"/corpqlo/static/inventory/" + str(filename), 'wb') as f:
                                		f.write(imgdata)
                        		record["file"] =  staticServer +"inventory/"+str(filename)  
					record["image"] = record["file"]
				print(record , "==========\n\n\n\n")
				rDict = PutData(collectionName, serializer, record, query=query)
				return Response(rDict)
			except Exception as e:
				rDict["status"] = "Error"
				rDict["ERROR"] = "Update error" + str(e)
				return Response(rDict)

		# if method is DELETE
		elif request.method == 'DELETE':
			'''Delete data'''
			# check parameters
			try:
				record = request.data
				print "record===============\n\n\n", record
				if("_id" in record ):
					pass
				else:
					raise Exception("Parameter missing")
			except Exception as e:
				rDict["ERROR:"] = "Parameters missing"
				rDict['e'] = str(e)
				return Response(rDict)

			query =  {
				"_id": ObjectId(record["_id"]),
				"email": record["email"]
			}
			
			try:
				_id = record["_id"]
				rDict = DeleteData(collectionName, serializer, record, query=query)
				print ("record after deleting=====",record)
				mg = mongoConnector.MongoConnector.getInstance()
                		dbc = mg.getDatabaseClient('corpqlo')
				try:
					mg.getCollection('stock').update(
                                                {"product_id":_id},
                                                {
                                                        "$set": {
                                                                "status": "deleted"
                                                        }
                                                },
                                                upsert= False
                                        )
					mg.getCollection('notifyme').update(
                                                {"product_id":_id},
                                                {
                                                        "$set": {
                                                                "status": "deleted"
                                                        }
                                                },
                                                upsert= False
                                        )

				except Exception as e:
					rDict = dict()
					rDict["status"] = "error"
					rDict["error"] = "error in deleting stock and notify me" + str(e) + str(record)

				
				print "record rDict return===============\n\n\n",rDict,record
				return Response(rDict)
			except Exception as e:
				rDict["status"] = "Error"
				rDict["ERROR"] = "Update error"
				return Response(rDict)

		# Otherwise raise error
		else:
			'''Raise error'''
			rDict["ERROR:"] = "Invalid request method"
			return Response(rDict)

	except Exception as e:
		rDict["ERROR:"] = "Internal server error" + str(e)
		return Response(rDict)

	return Response(rDict)

@api_view(['POST','GET', 'PUT' ,'DELETE'])
def Address(request):
	rDict = dict()
	rDict["status"] = "Error"
	collectionName = "address"
	serializer = AddressSerializer
	data = []
	query = {}
	filters = {}
	try:
		# If method is GET
		if request.method == 'GET':
			'''Get data'''
			# check
			try:
				numOfRecord = request.GET['noOfRecord']
				pageNumber = request.GET['pageNumber']
			except:
				numOfRecord = 999
				pageNumber = 1

			if request.GET.get('id'):
				query['_id'] = ObjectId(request.GET['id'])

			if request.GET.get('userId'):
				query['userId'] = request.GET['userId']

			rDict = GetData(collectionName, numOfRecord, pageNumber, query=query, filters=filters)
			if rDict["address"]:
				tmpList = []
				for i in rDict["address"]:
					if "status" in i.keys():
						continue
					tmpList.append(i)
				rDict["address"] = []
				rDict["address"] = tmpList

			if request.GET.get('minimalist'):
				filters = {
				}

		elif request.method == 'POST':
			'''Post data'''
			# check parameters
			try:
				record = request.data
				print "record============================\n\n\n",record
			except Exception as e:
				rDict["ERROR:"] = "Parameters missing"
				rDict['e'] = str(e)
				return Response(rDict)

			#rDict = lg.PostData(collectionName, serializer, record)
			query = record
			try:
				rDict = PostData(collectionName, serializer, record, query=query)
			except Exception as e:
				rDict["status"] = "Error"
				rDict["ERROR"] = "Post error"

		# if method is PUT
		elif request.method == 'PUT':
			'''Update data'''
			# check parameters
			try:
				record = request.data
				if("_id" in record and "owner" in record):
					pass
				else:
					raise Exception("Parameter missing")
			except Exception as e:
				rDict["ERROR:"] = "Parameters missing"
				rDict['e'] = str(e)
				return Response(rDict)

			query =  {
				"_id": ObjectId(record["_id"]),
				"owner": record["owner"]
			}
			try:
				rDict = PutData(collectionName, serializer, record, query=query)
				return Response(rDict)
			except Exception as e:
				rDict["status"] = "Error"
				rDict["ERROR"] = "Update error"
				return Response(rDict)

		# if method is DELETE
		elif request.method == 'DELETE':
			'''Delete data'''
			# check parameters
			try:
				record = request.data
				if("_id" in record and "email" in record):
					pass
				else:
					raise Exception("Parameter missing")
			except Exception as e:
				rDict["ERROR:"] = "Parameters missing"
				rDict['e'] = str(e)
				return Response(rDict)

			query =  {
				"_id": ObjectId(record["_id"]),
				"email": record["email"]
			}
			try:
				rDict = DeleteData(collectionName, serializer, record, query=query)
				return Response(rDict)
			except Exception as e:
				rDict["status"] = "Error"
				rDict["ERROR"] = "Update error"
				return Response(rDict)

		# Otherwise raise error
		else:
			'''Raise error'''
			rDict["ERROR:"] = "Invalid request method"
			return Response(rDict)

	except Exception as e:
		rDict["ERROR:"] = "Internal server error" + str(e)
		return Response(rDict)

	return Response(rDict)

#@csrf_exempt
@api_view(['POST','GET', 'PUT' ,'DELETE'])
def Orders(request):
	rDict = dict()
	rDict["status"] = "Error"
	collectionName = "order"
	serializer = OrderSerializer
	data = []
	query = {}
	filters = {}
	tmpDict = {}
	try:
		# If method is GET
		if request.method == 'GET':
			'''Get data'''
			# check
			try:
				numOfRecord = request.GET['noOfRecord']
				pageNumber = request.GET['pageNumber']
			except:
				numOfRecord = 999999999
				pageNumber = 1

			#if request.GET.get('email'):
			#	query['email'] = request.GET['email']
			
			if request.GET.get('minimalist'):
				filters = {
				}
			if request.GET.get('_id'):
				query['_id'] = ObjectId(request.GET['_id'])
			if request.GET.get('companyId'):
				tmpDict["companyId"] = request.GET['companyId']
				query['detailsOfProduct'] = {"$elemMatch":{"companyId":str(request.GET['companyId'])}}

			if request.GET.get('orderStatus'):
				if request.GET['orderStatus'] in ["pastOrders","delivered"]:
					query['deliveryStatus'] = "delivered"
				
				if request.GET['orderStatus'] in ["ongoing"]:
					query["deliveryStatus"] = {"$in":["orderPlaced","ready_for_pickup","shipped"]}
				else:
					query["deliveryStatus"] = {"$in":[request.GET['orderStatus']]}
			
				#else:
				#	query["status"] = 
			
			if request.GET.get('fromDate'):
				query["createdAt"] = {"$gte":datetime.strptime(request.GET["fromDate"],"%d-%m-%Y").replace(hour=0,minute=0)}
			#else:
			#	query["createdAt"] = {"$gte": datetime.today().replace(day=1,hour=0,minute=0)}
			if request.GET.get('toDate'):
				query["createdAt"]["$lte"] = datetime.strptime(request.GET["toDate"],"%d-%m-%Y").replace(hour=23,minute=59)
			#else:
			#	query["createdAt"] = {"$lte":datetime.today()}'''

			if request.GET.get('userId'):
				query['userId'] = request.GET['userId']
			if request.GET.get('email'):
				email = request.GET['email']

			rDict = GetData(collectionName, numOfRecord, pageNumber, query=query, filters=filters)
			if rDict["order"]:
				tmpList = []
				for i in  rDict["order"]:
					i["orderId"] = i["_id"][-6:]
					'''if "status" in i.keys():
						if i["status"] == "deleted":
							continue'''
					if "deliveryStatus" in i.keys():
						if i["deliveryStatus"] in ["delivered"]:
							i["rewardPoint"] = round(float(i["totalAmount"])/100)
							i["updatedStatus"] = "delivered"
						else:
							i["updatedStatus"] = "ongoing"
					else:
							i["updatedStatus"] = "ongoing"
					if i["detailsOfProduct"]:
						if len( i["detailsOfProduct"])>1:
							i["apparel"] = i["detailsOfProduct"][0]["name"] + " + "+str(len(i["detailsOfProduct"])-1)+"more"
						else:
							i["apparel"] = i["detailsOfProduct"][0]["name"]
						for j in i["detailsOfProduct"] :
							if "companyId" in tmpDict.keys():
								if j["companyId"] !=  tmpDict["companyId"] :
									del j
						
					if "lastUpdated" in i.keys():
						i["lastUpdated"] = i["lastUpdated"]
					else:
						i["lastUpdated"] = i["createdAt"]
					if isinstance(i["createdAt"],unicode):	
						createdAt =  datetime.strptime(i["createdAt"], '%Y-%m-%dT%H:%M:%S.%f')
						i["datetime"] =  datetime.strptime(i["createdAt"], '%Y-%m-%dT%H:%M:%S.%f')
					else:
						createdAt = i["createdAt"]
						i["datetime"] = i["createdAt"]
					'''if (datetime.today() - createdAt).days > 0:
						i["processing_datetime"] = datetime.today()'''

					'''if isinstance(i["createdAt"],unicode):
						try:
							i["createdAt"] = parse(str(i["createdAt"]))
						except:
							i["createdAt"] = str(i["createdAt"])
					if isinstance(i["createdAt"],datetime):
						i["createdAt"] = i["createdAt"].strftime("%s")'''

					tmpList.append(i)
				rDict["order"] = []
				if tmpList:
					tmpList.sort(key=lambda x:x["datetime"],reverse = True)
				rDict["order"] = tmpList

		# If method is POST
		elif request.method == 'POST':
			'''Post data'''
			# check parameters
			try:
				record = request.data
			except Exception as e:
				rDict["ERROR:"] = "Parameters missing"
				rDict['e'] = str(e)
				rDict['record'] = str(request.data)
				return Response(rDict)

			query = {}
			try:
				record["lastUpdated"] = datetime.today()
				for i in record["detailsOfProduct"]:
					if "stockId" in i.keys():
						qDict = dict()
						print('stockInfo' ,"========\n\n\n\n\n\n",qDict)
						qDict["_id"] = ObjectId(i["stockId"])
						stockInfo = GetData("stock", 9999, 1, query=qDict, filters={})
						print('stockInfo' , stockInfo ,"========\n\n\n\n\n\n",qDict)
						if stockInfo["stock"]:
							for j in stockInfo["stock"]:
								stockQty = 0
								if i["stockId"] == j["_id"]:
									stockQty	= int(j["stock_qty"]) - int(i["quantity"])
									print ("stock reduction ========\n\n\n\n\n\n\n\n\n\n",j["stock_qty"] ,"======================\n\n\n\n\n\n\n\n")
									if stockQty > -1:
										mg.getCollection('stock').update(
                                                        							{"_id":ObjectId(j["_id"])},
														{"$set":
                                                								 
														{"stock_qty": stockQty},
														},
                                                        							upsert= False
                                        									)

									

								if stockQty  < 0:
									rDict = dict()
									rDict["status"] = "error"
									print (j)
									rDict["error"] = "Requested size and color is not available in stock .Please select another color or click NotifyMe button in product screen to raise request"
									return Response(rDict)

								if int(j["stock_qty"]) < i["quantity"]:
									rDict = dict()
									rDict["status"] = "error"
									print (j)
									rDict["error"] = "The quantity available for this product is "  + str(int(j["stock_qty"])) + "Please add accordingly or choose another product"
									return Response(rDict)
									
				rDict = PostData(collectionName, serializer, record, query=query)
				rDict["message:"] = "Data inserted successfully"
				rDict['status'] = 'Success'
				rDict['record'] = str(record)
				return Response(rDict)
			except Exception as e:
				rDict['ERROR'] = "Data insertion error: " + str(e)
				rDict['status'] = 'Failed'
				rDict['record'] = str(record)
				return Response(rDict)

		# if method is PUT
		elif request.method == 'PUT':
			'''Update data'''
			# check parameters
			try:
				record = request.data
				print record
				if("_id" in record and "email" in record):
					pass
				else:
					raise Exception("Parameter missing")
			except Exception as e:
				rDict["ERROR:"] = "Parameters missing"
				rDict['e'] = str(e)
				return Response(rDict)

			query =  {
				"_id": ObjectId(record["_id"]),
				"email": record["email"]
			}
			try:
				if "file" in record.keys():
					imgdata = base64.b64decode(record["file"])
					filename = str(record["_id"])+ '.jpg'
                        		with open(os.getcwd() +"/corpqlo/static/order/" + str(filename), 'wb') as f:
                                		f.write(imgdata)
                        		record["file"] =  staticServer +"order/"+str(filename)  
					record["image"] = record["file"]

				rDict = PutData(collectionName, serializer, record, query=query)
				if record["deliveryStatus"]  == "delivered":
					updateDict = dict()
					updateDict["$inc"] = {"rewardPoint":(float(record["customer_paid_amount"])/100)}
					'''mg.getCollection('usercollection').update_many(
					{
                             			"userId": record["userId"],
                            		},
                    			updateDict,
                    			upsert= True
                    			)'''
					print updateDict

				return Response(rDict)
			except Exception as e:
				rDict["status"] = "Error"
				rDict["ERROR"] = "Update error" + str(e)
				return Response(rDict)

		# if method is DELETE
		elif request.method == 'DELETE':
			'''Delete data'''
			# check parameters
			try:
				record = request.data
				if("_id" in record and "userId" in record):
					pass
				else:
					raise Exception("Parameter missing")
			except Exception as e:
				rDict["ERROR:"] = "Parameters missing"
				rDict['e'] = str(e)
				return Response(rDict)

			query =  {
				"_id": ObjectId(record["_id"]),
				"userId": record["userId"]
			}
			try:
				rDict = DeleteData(collectionName, serializer, record, query=query)
				return Response(rDict)
			except Exception as e:
				rDict["status"] = "Error"
				rDict["ERROR"] = "Update error" + str(e)
				return Response(rDict)

		# Otherwise raise error
		else:
			'''Raise error'''
			rDict["ERROR:"] = "Invalid request method"
			rDict['status'] = 'Failed'
			return Response(rDict)

	except Exception as e:
		rDict["ERROR:"] = "Internal server error" + str(e)
		rDict['status'] = 'Failed'
		return Response(rDict)

	return Response(rDict)
@api_view(['POST','GET', 'PUT','DELETE'])
def Upload(request):
	rDict = dict()
	rDict["status"] = "Error"
	collectionName = "keyaccounts"
	serializer = KeyAccountSerializer
	data = []
	query = {}
	filters = {}
	try:
		# If method is GET
		if request.method == 'GET':
			'''Get data'''
			# check
			try:
				numOfRecord = request.GET['noOfRecord']
				pageNumber = request.GET['pageNumber']
			except:
				numOfRecord = 999
				pageNumber = 1

			if request.GET.get('owner'):
				query['owner'] = request.GET['owner']

			if request.GET.get('minimalist'):
				filters = {
				}
			if request.GET.get('id'):
				query['_id'] = ObjectId(request.GET['id'])

			rDict = GetData(collectionName, numOfRecord, pageNumber, query=query, filters=filters)
			if rDict["keyaccounts"]:
				tmpList = []
				for i in rDict["keyaccounts"]:
					if "status" in i.keys():
						if i["status"] == "deleted":
							continue
					tmpList.append(i)
				rDict["keyaccounts"] = tmpList

		# If method is POST
		elif request.method == 'POST':
			'''Post data'''
			# check parameters
			try:
				#if request.FILES["file"]:
				#	_file = request.FILES["file"]
				#	handle_uploaded_file(request.data["file"],"keyaccounts")
				record = dict()
				record = request.data
				#record = json.loads(record['body'])
				if "file" in record.keys():
					imgdata = base64.b64decode(record["file"])
					_file =  record["company"]  # I assume you have a way of picking unique filenames
					with open(os.getcwd() +"/corpqlo/static/keyaccounts/" + str(_file) + '.jpg', 'wb') as f:
    						f.write(imgdata)
					record["file"] =  staticServer + "keyaccounts/"+str(_file) + '.jpg'
				#handle_uploaded_file(_file)
				try:
					mg = mongoConnector.MongoConnector.getInstance()
					dbc = mg.getDatabaseClient('corpqlo')

					# check if data already exist
					if(query!={}):
						doc = mg.getCollection(collectionName).find(query).limit(1)
						docList = [i for i in doc]
						if docList:
							rDict["status"] = "Error"
							rDict["ERROR"] = "Data already exists"
							return Response(rDict)
				except Exception as e:
					rDict["ERROR:"] = str(e)
					rDict["status"] = "Error"
					return Response(rDict)

				# insert data
				try:
					rDict["createdAt"] = datetime.today()
					rDict["id"] = str(mg.insertCollection(collectionName, record))
					rDict["status"] = "Success"
					print "data is inserted"
					return Response(rDict)
				except:
					rDict["ERROR"] = "Database insert error"
					rDict["status"] = "Error"
					return Response(rDict)
				print "record=========================",rDict
			except Exception as e:
				rDict["ERROR:"] = "Parameters missing"
				rDict['e'] = str(e)
				return Response(rDict)

			query = {}
			try:
				rDict = PostData(collectionName, serializer, record, query=query)
				rDict["message:"] = "Data inserted successfully"
				rDict['status'] = 'Success'
				rDict['record'] = str(record)
				return Response(rDict)
			except Exception as e:
				rDict['ERROR'] = "Data insertion error: " + str(e)
				rDict['status'] = 'Failed'
				rDict['record'] = str(record)
				return Response(rDict)

		# if method is PUT
		elif request.method == 'PUT':
			'''Update data'''
			# check parameters
			try:
				record = request.data
				if("_id" in record and "email" in record):
					pass
				else:
					raise Exception("Parameter missing")
			except Exception as e:
				rDict["ERROR:"] = "Parameters missing"
				rDict['e'] = str(e)
				return Response(rDict)
			if "file" in record.keys():
				imgdata = base64.b64decode(record["file"])
				if "company" in record.keys():
					filename = record["company"] + str(random.choice([1, 4, 8, 10, 3]))+ '.jpg'  # I assume you have a way of picking unique filenames
				else:
					filename = "keyaccount" + str(random.choice([1, 4, 8, 10, 3]))+ '.jpg'  # I assume you have a way of picking unique filenames
				with open(os.getcwd() +"/corpqlo/static/keyaccounts/" + str(filename), 'w+') as f:
    					f.write(imgdata)
				record["file"] =  staticServer +"keyaccounts/"+str(filename)

				

			query =  {
				"_id": ObjectId(record["_id"]),
				"email": record["email"]
			}
			try:
				rDict = PutData(collectionName, serializer, record, query=query)
				return Response(rDict)
			except Exception as e:
				rDict["status"] = "Error"
				rDict["ERROR"] = "Update error" + str(e)
				return Response(rDict)

		# if method is DELETE
		elif request.method == 'DELETE':
			'''Delete data'''
			# check parameters
			try:
				record = request.data
				if("_id" in record and "email" in record):
					pass
				else:
					raise Exception("Parameter missing")
			except Exception as e:
				rDict["ERROR:"] = "Parameters missing"
				rDict['e'] = str(e)
				return Response(rDict)

			query =  {
				"_id": ObjectId(record["_id"]),
				"email": record["email"]
			}
			try:
				rDict = DeleteData(collectionName, serializer, record, query=query)
				return Response(rDict)
			except Exception as e:
				rDict["status"] = "Error"
				rDict["ERROR"] = "Update error"  +str(e)
				return Response(rDict)

		# Otherwise raise error
		else:
			'''Raise error'''
			rDict["ERROR:"] = "Invalid request method"
			rDict['status'] = 'Failed'
			return Response(rDict)

	except Exception as e:
		rDict["ERROR:"] = "Internal server error" + str(e)
		rDict['status'] = 'Failed'
		return Response(rDict)

	return Response(rDict)
@api_view(['POST','GET', 'PUT'])
def ChangePassword(request):
	rJson = dict()
	rJson["status"] = "Error"
	if request.method == 'GET':
		reqDict = request.GET
	elif request.method == 'POST':
		reqDict = request.POST
	record = dict()
	record["username"] = reqDict.get("email")
	if reqDict.get("newPassword"):
		record["newPassword"] = reqDict.get("newPassword")
	if reqDict.get("oldPassword"):
		record["oldPassword"] =  reqDict['oldPassword']
	try:
		userObj = User.objects.get(username= record["username"])
	except Exception as e :
		rJson["ERROR"] = "Username not exist" + str(e)
		return Response(rJson)
	if "oldPassword" in record.keys():
		if not userObj.check_password(record["oldPassword"]):
			rJson["ERROR"] = "wrong old password"
			return Response(rJson)
	userObj.set_password(record["newPassword"])
	userObj.save()	
	rJson["status"] = "success"
	return Response(rJson)

@api_view(['POST','GET', 'PUT'])
def ForgotPassword(request):
	rJson = dict()
	rJson["status"] = "Error"
	record = dict()
	if request.method == 'GET':
		reqDict = request.GET
		record["username"] = reqDict.get('email')
	elif request.method == 'POST':
		reqDict = request.POST
		record["username"] = reqDict.get('email')
	rJson["otp"] = random.randint(1000,9999)
	params = dict()
	params["mList"] = record["username"]
	params["subject"] = "Reset Password OTP verification - Corpqlo"
	params["message"] = "Please verify your reset password request using the OTP mentioned below.\n\n\n\n OTP:" + str(rJson["otp"])
	try:
		CommunicationConnector('resetPassword',params,[])
	except:
		rJson["ERROR"] = "failed in email verification"

	try:
		#userObj = User.objects.get(username= record["username"])
		userObj = GetData('usercollection',1,1,query = {"email":record["username"]},filters = {})
		if not userObj["usercollection"]:
			rJson["ERROR"] = "Username not exist"
			return Response(rJson)
	except:
		rJson["ERROR"] = "Username not exist"
		rJson["ERROR"] = str(record)
		return Response(rJson)
	rJson["status"] = "success"
	return Response(rJson)

@api_view(['POST','GET', 'PUT'])
def Logout(request):
	if request.method == 'GET':
		reqDict = request.GET
	elif request.method == 'POST':
		reqDict = request.POST
	record = dict()
	record["token"] = reqDict['token']
	user = Token.objects.get(key=record["token"]).delete()
	print "user",user
	return Response({"status":"success"})
@api_view(['POST','GET', 'PUT'])
def Premium(request):
	rDict = dict()
	collectionName = "membership"
	serializer = MembershipSerializer
	if request.method == 'GET':
		_mList = []
		if request.GET.get('userId'):
			if request.GET.get('userId') == "All":
				_mList = GetData('membership',99999,1, query={}, filters= {})
				if _mList["membership"]:
					rDict["subscription"] = []
					tmpDict = dict()
					for i in _mList["membership"]:
						if i["userId"] not in tmpDict.keys():
							tmpDict[i["userId"]] = i
						else:
							if tmpDict[i["userId"]]["createdAt"] < i["createdAt"]:
								del tmpDict[i["userId"]]
								tmpDict[i["userId"]] = i
					for k , v in tmpDict.iteritems():
						rDict["subscription"].append(v)
					return Response(rDict)
					
			else:
				_mList = GetData('membership',1,50, query={"userId":request.GET["userId"]}, filters= {})
				if _mList["membership"]:
					rDict["subscription"] = [_mList["membership"][-1]]
					return Response(rDict)

		#rDict["subscription"] = json.loads(open('corpqlo/static/params/params.json').read())["premium"]
		'''if _mList:
			tmpList = []
			for i in rDict["subscription"]:
				i["membershipId"] = i["_id"]
				for j in range(0,len(_mList["membership"])):
					if i["type"] == _mList["membership"][j]["type"]:
						i["status"] = "paid"
						tmpList.append(i)
						continue
					else:
						tmpList.append(i)
				tmpList.append(i)
			rDict["subscription"] = []
			rDict["subscription"] = tmpList'''
			
	elif request.method == 'POST':
		try:
			record = request.data
			record["startDate"] = datetime.today()
		except Exception as e:
			rDict["ERROR:"] = "Parameters missing"
			rDict['e'] = str(e)
			return Response(rDict)

		query = {}
		try:
			print "record================\n\n\n",record
			rDict = PostData(collectionName, serializer, record, query=query)
			rDict["status"] = "Success"
		except Exception as e:
			rDict["status"] = "Error"
			rDict["ERROR:"] = "Post error " +str(e)
			return Response(rDict)
	elif request.method == 'PUT':
			'''Update data'''
			# check parameters
			try:
				record = request.data
				if("_id" in record and "email" in record):
					pass
				else:
					raise Exception("Parameter missing")
			except Exception as e:
				rDict["ERROR:"] = "Parameters missing"
				rDict['e'] = str(e)
				return Response(rDict)

			query =  {
				"_id": ObjectId(record["_id"]),
				"email": record["email"]
			}
			try:
				rDict = PutData(collectionName, serializer, record, query=query)
				return Response(rDict)
			except Exception as e:
				rDict["status"] = "Error"
				rDict["ERROR"] = "Update error"
				return Response(rDict)

		
	return Response(rDict)
#@csrf_exempt
@api_view(['POST','GET', 'PUT' ,'DELETE'])
def Stock(request):
	rDict = dict()
	rDict["status"] = "Error"
	collectionName = "stock"
	serializer = ProductSerializer
	data = []
	query = {}
	filters = {}
	numOfRecord = 9999
	pageNumber = 1
	try:
		# If method is GET
		if request.method == 'GET':
			'''Get data'''
			# check
			try:
				numOfRecord = request.GET['noOfRecord']
				pageNumber = request.GET['pageNumber']
			except:
				numOfRecord = 9999
				pageNumber = 1

			#if request.GET.get('email'):
			#	query['email'] = request.GET['email']
			
			if request.GET.get('minimalist'):
				filters = {
				}
			if request.GET.get('_id'):
				query['_id'] = ObjectId(request.GET['_id'])
			if request.GET.get('product_id'):
				query['product_id'] = request.GET['product_id']
			rDict = GetData(collectionName, numOfRecord, pageNumber, query=query, filters=filters)
			if rDict["stock"]:
				tmpList = []
				for i in  rDict["stock"]:
					if "status" in i.keys():
						if i["status"] == "deleted":
							continue
                                                '''qDict = dict()
                                                qDict["datetime"] = {"$gte":i["datetime"]}
                                                qDict["detailsOfProduct.product_stock.stockId"] = i["_id"]
                                                orderInfo = GetData("order", numOfRecord, pageNumber, query=qDict, filters={})
                                                if orderInfo["order"]:
							for k in orderInfo["order"]:
								for m in k["detailsOfProduct"]:
									if m["product_stock"]["stockId"] == i["_id"]:
										i["stock_qty"]  = int(i["stock_qty"]) - int(m["quantity"])'''

					tmpList.append(i)
				rDict["stock"] = []
				rDict["stock"] = tmpList

		# If method is POST
		elif request.method == 'POST':
			'''Post data'''
			# check parameters
			try:
				record = request.data
			except Exception as e:
				rDict["ERROR:"] = "Parameters missing"
				rDict['e'] = str(e)
				rDict['record'] = str(request.data)
				return Response(rDict)

			query = {}
			try:
				record["lastUpdated"] = datetime.today()
				rDict = PostData(collectionName, serializer, record, query=query)
				rDict["message:"] = "Data inserted successfully"
				rDict['status'] = 'Success'
				rDict['record'] = str(record)
				print(rDict)
				return Response(rDict)
			except Exception as e:
				rDict['ERROR'] = "Data insertion error: " + str(e)
				rDict['status'] = 'Failed'
				rDict['record'] = str(record)
				return Response(rDict)

		# if method is PUT
		elif request.method == 'PUT':
			'''Update data'''
			# check parameters
			try:
				record = request.data
				if("_id" in record and "email" in record):
					pass
				else:
					raise Exception("Parameter missing")
			except Exception as e:
				rDict["ERROR:"] = "Parameters missing"
				rDict['e'] = str(e)
				return Response(rDict)

			query =  {
				"_id": ObjectId(record["_id"]),
				"email": record["email"]
			}
			try:
				qDict = dict()
				if "stock_id" in record.keys():
					qDict["stock_id"] = record["_id"]
				
				notifyDict = GetData("notifyme", numOfRecord, pageNumber, query=qDict, filters=filters)
				stockDict = GetData("stock", numOfRecord, pageNumber, query={"_id": ObjectId(record["_id"])}, filters=filters)
				if stockDict["stock"]:
					stockById = stockDict["stock"][0]
				if notifyDict["notifyme"] :
					for i in notifyDict["notifyme"]:
						if "status" in i.keys():
							if i["status"] == "deleted":
								continue
						if i["datetime"] > stockById["datetime"]:
							continue
						if "email" in i.keys():
							params = {}
							params["mList"] = i["email"]
							if "size" in i.keys() and "color" in i.keys():
								params["message"] = i["name"] +" - " +i["size"] + "," + i["color"] + " stock added, you can buy the product now."
							elif "color" in i.keys() :
								params["message"] = i["name"] +" - "+ i["color"] + " stock added, you can buy the product now." 
							else:
								params["message"] = i["name"] + " stock added, you can buy the product now." 
							params["subject"] = "Corpqlo - Your requested stock is available to order"
							try:
								CommunicationConnector('notifyme',params,[])
							except:
								rDict["error"] = "error in mail communication"

				rDict = PutData(collectionName, serializer, record, query=query)
				return Response(rDict)
			except Exception as e:
				rDict["status"] = "Error"
				rDict["ERROR"] = "Update error" + str(e)
				return Response(rDict)

		# if method is DELETE
		elif request.method == 'DELETE':
			'''Delete data'''
			# check parameters
			try:
				record = request.data
				print(record)
				if("_id" in record and "email" in record):
					pass
				else:
					raise Exception("Parameter missing")
			except Exception as e:
				rDict["ERROR:"] = "Parameters missing"
				rDict['e'] = str(e)
				return Response(rDict)

			query =  {
				"_id": ObjectId(record["_id"]),
				"email": record["email"]
			}
			try:
				rDict = DeleteData(collectionName, serializer, record, query=query)
				return Response(rDict)
			except Exception as e:
				rDict["status"] = "Error"
				rDict["ERROR"] = "Update error"
				return Response(rDict)

		# Otherwise raise error
		else:
			'''Raise error'''
			rDict["ERROR:"] = "Invalid request method"
			rDict['status'] = 'Failed'
			return Response(rDict)

	except Exception as e:
		rDict["ERROR:"] = "Internal server error" + str(e)
		rDict['status'] = 'Failed'
		return Response(rDict)

	return Response(rDict)


@api_view(['POST','GET', 'PUT' ,'DELETE'])
def NotifyMe(request):
	rDict = dict()
	rDict["status"] = "Error"
	collectionName = "notifyme"
	serializer = AddressSerializer
	data = []
	query = {}
	filters = {}
	try:
		# If method is GET
		if request.method == 'GET':
			'''Get data'''
			# check
			try:
				numOfRecord = request.GET['noOfRecord']
				pageNumber = request.GET['pageNumber']
			except:
				numOfRecord = 50
				pageNumber = 1

			if request.GET.get('id'):
				query['_id'] = ObjectId(request.GET['id'])

			if request.GET.get('userId'):
				query['userId'] = request.GET['userId']

			rDict = GetData(collectionName, numOfRecord, pageNumber, query=query, filters=filters)
			if rDict["notifyme"]:
				tmpList = []
				for i in rDict["notifyme"]:
					if "status" in i.keys():
						continue
					tmpList.append(i)
				rDict["notifyme"] = []
				rDict["notifyme"] = tmpList

			if request.GET.get('minimalist'):
				filters = {
				}

		elif request.method == 'POST':
			'''Post data'''
			# check parameters
			try:
				record = request.data
				print "record============================\n\n\n",record
			except Exception as e:
				rDict["ERROR:"] = "Parameters missing"
				rDict['e'] = str(e)
				return Response(rDict)

			#rDict = lg.PostData(collectionName, serializer, record)
			query = record
			try:
				rDict = PostData(collectionName, serializer, record, query=query)
			except Exception as e:
				rDict["status"] = "Error"
				rDict["ERROR"] = "Post error"

		# if method is PUT
		elif request.method == 'PUT':
			'''Update data'''
			# check parameters
			try:
				record = request.data
				if("_id" in record and "owner" in record):
					pass
				else:
					raise Exception("Parameter missing")
			except Exception as e:
				rDict["ERROR:"] = "Parameters missing"
				rDict['e'] = str(e)
				return Response(rDict)

			query =  {
				"_id": ObjectId(record["_id"]),
				"owner": record["owner"]
			}
			try:
				rDict = PutData(collectionName, serializer, record, query=query)
				return Response(rDict)
			except Exception as e:
				rDict["status"] = "Error"
				rDict["ERROR"] = "Update error"
				return Response(rDict)

		# if method is DELETE
		elif request.method == 'DELETE':
			'''Delete data'''
			# check parameters
			try:
				record = request.data
				if("_id" in record and "email" in record):
					pass
				else:
					raise Exception("Parameter missing")
			except Exception as e:
				rDict["ERROR:"] = "Parameters missing"
				rDict['e'] = str(e)
				return Response(rDict)

			query =  {
				"_id": ObjectId(record["_id"]),
				"email": record["email"]
			}
			try:
				rDict = DeleteData(collectionName, serializer, record, query=query)
				return Response(rDict)
			except Exception as e:
				rDict["status"] = "Error"
				rDict["ERROR"] = "Update error"
				return Response(rDict)

		# Otherwise raise error
		else:
			'''Raise error'''
			rDict["ERROR:"] = "Invalid request method"
			return Response(rDict)

	except Exception as e:
		rDict["ERROR:"] = "Internal server error" + str(e)
		return Response(rDict)

	return Response(rDict)

@api_view(['POST','GET', 'PUT' ,'DELETE'])
def Contacts(request):
	rDict = dict()
	rDict["status"] = "Error"
	collectionName = "contacts"
	serializer = AddressSerializer
	data = []
	query = {}
	filters = {}
	try:
		# If method is GET
		if request.method == 'GET':
			'''Get data'''
			# check
			try:
				numOfRecord = request.GET['noOfRecord']
				pageNumber = request.GET['pageNumber']
			except:
				numOfRecord = 50
				pageNumber = 1

			if request.GET.get('id'):
				query['_id'] = ObjectId(request.GET['id'])

			if request.GET.get('userId'):
				query['userId'] = request.GET['userId']

			rDict = GetData(collectionName, numOfRecord, pageNumber, query=query, filters=filters)
			if rDict["contacts"]:
				tmpList = []
				for i in rDict["contacts"]:
					if "status" in i.keys():
						continue
					tmpList.append(i)
				rDict["contacts"] = []
				if tmpList:
					rDict["contacts"] = tmpList[-1]

			if request.GET.get('minimalist'):
				filters = {
				}

		elif request.method == 'POST':
			'''Post data'''
			# check parameters
			try:
				record = request.data
				print "record============================\n\n\n",record
			except Exception as e:
				rDict["ERROR:"] = "Parameters missing"
				rDict['e'] = str(e)
				return Response(rDict)

			#rDict = lg.PostData(collectionName, serializer, record)
			query = record
			try:
				rDict = PostData(collectionName, serializer, record, query=query)
			except Exception as e:
				rDict["status"] = "Error"
				rDict["ERROR"] = "Post error"

		# if method is PUT
		elif request.method == 'PUT':
			'''Update data'''
			# check parameters
			try:
				record = request.data
				if("_id" in record and "email" in record):
					pass
				else:
					raise Exception("Parameter missing")
			except Exception as e:
				rDict["ERROR:"] = "Parameters missing"
				rDict['e'] = str(e)
				return Response(rDict)

			query =  {
				"_id": ObjectId(record["_id"]),
				"email": record["email"]
			}
			try:
				rDict = PutData(collectionName, serializer, record, query=query)
				return Response(rDict)
			except Exception as e:
				rDict["status"] = "Error"
				rDict["ERROR"] = "Update error"
				return Response(rDict)

		# if method is DELETE
		elif request.method == 'DELETE':
			'''Delete data'''
			# check parameters
			try:
				record = request.data
				if("_id" in record and "email" in record):
					pass
				else:
					raise Exception("Parameter missing")
			except Exception as e:
				rDict["ERROR:"] = "Parameters missing"
				rDict['e'] = str(e)
				return Response(rDict)

			query =  {
				"_id": ObjectId(record["_id"]),
				"email": record["email"]
			}
			try:
				rDict = DeleteData(collectionName, serializer, record, query=query)
				return Response(rDict)
			except Exception as e:
				rDict["status"] = "Error"
				rDict["ERROR"] = "Update error"
				return Response(rDict)

		# Otherwise raise error
		else:
			'''Raise error'''
			rDict["ERROR:"] = "Invalid request method"
			return Response(rDict)

	except Exception as e:
		rDict["ERROR:"] = "Internal server error" + str(e)
		return Response(rDict)

	return Response(rDict)

@api_view(['POST','GET', 'PUT' ,'DELETE'])
def Packages(request):
	rDict = dict()
	rDict["status"] = "Error"
	collectionName = "packages"
	serializer = AddressSerializer
	data = []
	query = {}
	filters = {}
	try:
		# If method is GET
		if request.method == 'GET':
			'''Get data'''
			# check
			try:
				numOfRecord = request.GET['noOfRecord']
				pageNumber = request.GET['pageNumber']
			except:
				numOfRecord = 50
				pageNumber = 1

			if request.GET.get('id'):
				query['_id'] = ObjectId(request.GET['id'])

			if request.GET.get('userId'):
				query['userId'] = request.GET['userId']

			rDict = GetData(collectionName, numOfRecord, pageNumber, query=query, filters=filters)
			if rDict["packages"]:
				tmpList = []
				for i in rDict["packages"]:
					if "status" in i.keys():
						continue
					tmpList.append(i)
				rDict["packages"] = []
				if tmpList:
					rDict["packages"] = tmpList

			if request.GET.get('minimalist'):
				filters = {
				}

		elif request.method == 'POST':
			'''Post data'''
			# check parameters
			try:
				record = request.data
				print "record============================\n\n\n",record
			except Exception as e:
				rDict["ERROR:"] = "Parameters missing"
				rDict['e'] = str(e)
				return Response(rDict)

			#rDict = lg.PostData(collectionName, serializer, record)
			query = record
			try:
				rDict = PostData(collectionName, serializer, record, query=query)
			except Exception as e:
				rDict["status"] = "Error"
				rDict["ERROR"] = "Post error"

		# if method is PUT
		elif request.method == 'PUT':
			'''Update data'''
			# check parameters
			try:
				record = request.data
				if("_id" in record and "email" in record):
					pass
				else:
					raise Exception("Parameter missing")
			except Exception as e:
				rDict["ERROR:"] = "Parameters missing"
				rDict['e'] = str(e)
				return Response(rDict)

			query =  {
				"_id": ObjectId(record["_id"]),
				"email": record["email"]
			}
			try:
				rDict = PutData(collectionName, serializer, record, query=query)
				return Response(rDict)
			except Exception as e:
				rDict["status"] = "Error"
				rDict["ERROR"] = "Update error"
				return Response(rDict)

		# if method is DELETE
		elif request.method == 'DELETE':
			'''Delete data'''
			# check parameters
			try:
				record = request.data
				if("_id" in record and "email" in record):
					pass
				else:
					raise Exception("Parameter missing")
			except Exception as e:
				rDict["ERROR:"] = "Parameters missing"
				rDict['e'] = str(e)
				return Response(rDict)

			query =  {
				"_id": ObjectId(record["_id"]),
				"email": record["email"]
			}
			try:
				rDict = DeleteData(collectionName, serializer, record, query=query)
				return Response(rDict)
			except Exception as e:
				rDict["status"] = "Error"
				rDict["ERROR"] = "Update error"
				return Response(rDict)

		# Otherwise raise error
		else:
			'''Raise error'''
			rDict["ERROR:"] = "Invalid request method"
			return Response(rDict)

	except Exception as e:
		rDict["ERROR:"] = "Internal server error" + str(e)
		return Response(rDict)

	return Response(rDict)

@api_view(['POST','GET', 'PUT' ,'DELETE'])
def Report(request):
	rDict = dict()
	qDict = dict()
	collectionName = "order"
	numOfRecord = 99999
	pageNumber = 1
	filters = {}
	checkDict = dict()
	dDict= {}
	qDict["updatedStatus"] = "delivered"
	try:
		if request.method == 'GET':
			if request.GET.get('userId'):
				qDict["userId"] = request.GET["userId"]
			if request.GET.get('productId'):
				qDict["detailsOfProduct.product_id"] = {"$in":[request.GET['productId']]}
				checkDict["product_id"] = [request.GET['productId']]
			if request.GET.get('size'):
				qDict["detailsOfProduct.size"] = {"$in":ast.literal_eval(request.GET['size'])}
				checkDict["size"] = [ast.literal_eval(request.GET['size'])]
			if request.GET.get('stockId'):
				qDict["detailsOfProduct.stockId"] = {"$in":ast.literal_eval(request.GET['stockId'])}
				checkDict["stockId"] = [request.GET['stockId']]
			if request.GET.get('fromDate'):
				#qDict["createdAt"] = {}
				#qDict["createdAt"]["$gte"] = datetime.strptime(request.GET["fromDate"],"%d-%m-%Y").replace(hour=0,minute=0,second=1,microsecond=1)
				dDict["startDate"] = datetime.strptime(request.GET["fromDate"],"%d-%m-%Y").replace(hour=0,minute=0,second=1,microsecond=1)
			if request.GET.get('toDate'):
				#qDict["createdAt"]["$lte"] = datetime.strptime(request.GET["toDate"],"%d-%m-%Y").replace(hour=23,minute=59,second=59,microsecond=59)
				dDict["endDate"] = datetime.strptime(request.GET["toDate"],"%d-%m-%Y").replace(hour=23,minute=59,second=59,microsecond=59)
			#	qDict["createdAt"] = {"$lte":datetime.today()}
		print qDict ,"qDict*************=====================\n\n\n\n\n"
		rDict = GetData(collectionName, numOfRecord, pageNumber, query=qDict, filters=filters)
		print len(rDict["order"]),"=====\n\n\n\n\n"
		if rDict["order"]:
			reportDict = dict()
			oList = []
			for i in rDict["order"]:
				if isinstance(i["createdAt"],unicode):
					i["createdAt"] = datetime.strptime(i["createdAt"],'%Y-%m-%dT%H:%M:%S.%f')
				if "startDate" in dDict.keys() and "endDate" in dDict.keys():
					if  i["createdAt"] > dDict["endDate"] or i["createdAt"] < dDict["startDate"]:
						print dDict["startDate"] , i["createdAt"] ,dDict["endDate"]
						continue
				for j in i["detailsOfProduct"]:
					tmpDict = {}
					tmpDict["userId"]= i["userId"]
					if "product_id" not in j.keys():
						continue
					if "stockId" not in j.keys():
						continue
					if "product_id" in checkDict.keys():
						if j["product_id"] not in checkDict["product_id"]:
							continue
					if "stockId" in checkDict.keys():
						if j["stockId"] not in checkDict["stockId"]:
							continue
					if "size" in checkDict.keys():
						if j["size"] not in checkDict["size"]:
							continue
					tmpDict["productId"]= j["product_id"]
					tmpDict["stockId"]= j["stockId"]
					tmpDict["size"]= j["size"]
					tmpDict["color"]= j["color"]
					tmpDict["productName"]= j["name"]
					tmpDict["quantity"]= int(j["quantity"])
					tmpDict["price"]= float(j["price"])
					tmpDict["createdAt"]= i["createdAt"]
					tmpDict["createdAt"]= i["createdAt"]
					if "updatedStatus" in i.keys():
						tmpDict["deliveryStatus"]= i["updatedStatus"]
					if "userName" in i.keys():
						tmpDict["customerName"] = i["userName"]
					if "mobile" in i.keys():
						tmpDict["phone"] = i["mobile"]
					if "email" in i.keys():
						tmpDict["email"] = i["email"]
					tmpDict["orderId"] = i["_id"]
					tmpDict["paymentMode"] = i["paymentMode"]
					tmpDict["deliveryAddress"] = str(i["deliveryAddress"])
					oList.append(tmpDict)
			if oList:
				df =  pd.DataFrame(oList)
				print(df.columns)
				aggregate = df.groupby(['productId','stockId','productName','size','color','createdAt',"customerName","phone","email","orderId","paymentMode","deliveryAddress","deliveryStatus"]).agg({'price':'sum','quantity':'sum'})
				aggToDict = aggregate.to_dict(orient='dict')
				oList = []
				for i in aggToDict["price"].keys():
					tmpDict = dict()
					tmpDict["productId"] = i[0]
					tmpDict["stockId"] = i[1]
					tmpDict["name"] = i[2]
					tmpDict["size"] = i[3]
					tmpDict["color"] = i[4]
					tmpDict["createdAt"] = i[5]
					tmpDict["orderedAt"] = i[5]
					tmpDict["customerName"] = i[6]
					tmpDict["phone"] = i[7]
					tmpDict["email"] = i[8]
					tmpDict["orderId"] = i[9]
					tmpDict["paymentMode"] = i[10]
					tmpDict["deliveryAddress"] = ast.literal_eval(i[11])
					tmpDict["deliveryStatus"] = i[12]
					tmpDict["price"] = aggToDict["price"][i]
					tmpDict["quantity"] = aggToDict["quantity"][i]
					#tmpDict["customerName"] = aggToDict["customerName"][i]
					oList.append(tmpDict)
			rDict["order"] = []
			rDict["order"] = oList
					
	except Exception as e:
		rDict = dict()
		rDict["status"] = "error"
		rDict["error"] = "Parameter missing " + str(e)
		print (rDict)

	return Response(rDict)

@api_view(['POST','GET', 'PUT' ,'DELETE'])
def Measurment(request):
	rDict = dict()
	rDict["status"] = "Error"
	collectionName = "measurment"
	serializer = AddressSerializer
	data = []
	query = {}
	filters = {}
	try:
		# If method is GET
		if request.method == 'GET':
			'''Get data'''
			# check
			try:
				numOfRecord = request.GET['noOfRecord']
				pageNumber = request.GET['pageNumber']
			except:
				numOfRecord = 50
				pageNumber = 1

			if request.GET.get('id'):
				query['_id'] = ObjectId(request.GET['id'])

			if request.GET.get('userId'):
				query['userId'] = request.GET['userId']

			rDict = GetData(collectionName, numOfRecord, pageNumber, query=query, filters=filters)
			if rDict["measurment"]:
				tmpList = []
				userList = []
				for i in rDict["measurment"]:
					if "status" in i.keys():
						if i["status"] == "deleted":
							continue
					userList.append(i["userId"])
					tmpList.append(i)
				rDict["measurment"] = []
				rDict["measurment"] = tmpList
			if request.GET.get('minimalist'):
				query = dict()
				del rDict["measurment"]
				if userList:
					query["_id"] = {"$in":[ObjectId(i) for i in userList]}
					rDict = GetData("usercollection", numOfRecord, pageNumber, query=query, filters=filters)


		elif request.method == 'POST':
			'''Post data'''
			# check parameters
			try:
				record = request.data
				print "record============================\n\n\n",record
			except Exception as e:
				rDict["ERROR:"] = "Parameters missing"
				rDict['e'] = str(e)
				return Response(rDict)

			#rDict = lg.PostData(collectionName, serializer, record)
			query = record
			try:
				rDict = PostData(collectionName, serializer, record, query=query)
			except Exception as e:
				rDict["status"] = "Error"
				rDict["ERROR"] = "Post error"

		# if method is PUT
		elif request.method == 'PUT':
			'''Update data'''
			# check parameters
			try:
				record = request.data
				if("_id" in record and "email" in record):
					pass
				else:
					raise Exception("Parameter missing")
			except Exception as e:
				rDict["ERROR:"] = "Parameters missing"
				rDict['e'] = str(e)
				return Response(rDict)

			query =  {
				"_id": ObjectId(record["_id"]),
				"email": record["email"]
			}
			try:
				rDict = PutData(collectionName, serializer, record, query=query)
				return Response(rDict)
			except Exception as e:
				rDict["status"] = "Error"
				rDict["ERROR"] = "Update error"
				return Response(rDict)

		# if method is DELETE
		elif request.method == 'DELETE':
			'''Delete data'''
			# check parameters
			try:
				record = request.data
				if("_id" in record and "email" in record):
					pass
				else:
					raise Exception("Parameter missing")
			except Exception as e:
				rDict["ERROR:"] = "Parameters missing"
				rDict['e'] = str(e)
				return Response(rDict)

			query =  {
				"_id": ObjectId(record["_id"]),
				"email": record["email"]
			}
			try:
				rDict = DeleteData(collectionName, serializer, record, query=query)
				return Response(rDict)
			except Exception as e:
				rDict["status"] = "Error"
				rDict["ERROR"] = "Update error"
				return Response(rDict)

		# Otherwise raise error
		else:
			'''Raise error'''
			rDict["ERROR:"] = "Invalid request method"
			return Response(rDict)

	except Exception as e:
		rDict["ERROR:"] = "Internal server error" + str(e)
		return Response(rDict)

	return Response(rDict)


