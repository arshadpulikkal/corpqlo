from corpQlo.dataconnector import mongoConnector
from corpQlo.errcode import errcode as err
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
#import sys
#sys.path.insert(0,"../../server/antlegs_site/antlegs/")
#from serializers import JobSerializer, CandidateSerializer, JobToCandidateSerializer
#from models import JobModel, CandidateModel, JobToCandidateModel
#from google.oauth2 import service_account
#import googleapiclient.discovery
from datetime import datetime
from django.utils.dateparse import parse_datetime
from bson.objectid import ObjectId
class Logic:
	def __init__(self):
		self.dbName = "corpqlo"
		#service = self.GetGoogleCelender()
		pass
	def GetId(self,collectionName,rule,data):
		rDict = dict()
		rDict["status"] = "success"
                dbh = mongoConnector.MongoConnector.getInstance()
                dbc = dbh.getDatabaseClient(self.dbName)
                if dbc is None:
                        #print err.errdict["err_db_none"] % self.dbname
                        return
                collection = dbh.getCollection(collectionName)
                if collection is None:
                        #print err.errdict["err_collection_none"] % 'usercollection'
                        return
		queryDict = dict()
		print "GetInsidefunctiom"
		if rule == "duplicate":
			queryDict["owner"] = data["owner"]
			queryDict["email"] = data["email"]
			#queryDict["contactNo"] = data["contactNo"]
			docs = collection.find(queryDict,{"_id":False})
			docList = [i for i in docs]
			if docList:
				rDict["status"] = "Failed"
				rDict["error"] = data["email"] + "  email already added"
				return rDict

		queryDict = dict()
		docs = collection.find(queryDict,{"_id":False}).sort([("date",-1)]).limit(1)
		docList = [i for i in docs]
		if docList:
			_sList= docList[0]["aId"].split('-')
			aId =_sList[0] + "-"+ str(int(_sList[1])+1)
			print collectionName, docList,"in if",aId
		else:
			print collectionName, docList,"in else"
			if collectionName == "job":
				aId = "ALJ-100"
			elif collectionName == 'candidate':
				aId = "ALC-100"
			elif collectionName == 'interview':
				aId = "ALIn-100"
			else:
				aId = "ALI-100"
		rDict["aId"] = aId
		return rDict
	def GetData(self,collectionName,query ,Filter,rule):
		dbh = mongoConnector.MongoConnector.getInstance()
                dbc = dbh.getDatabaseClient(self.dbName)
                if dbc is None:
                        print err.errDict["err_db_none"] % self.dbname
                        return
                collection = dbh.getCollection(collectionName)
                if collection is None:
                        print err.errDict["err_collection_none"] % collectionName
                        return
		if rule == "jobId":
			docs = collection.aggregate([{"$match":{"owner":query["owner"]}},{"$group":{"_id":1,"jId":{"$addToSet":"$jobId"}}}])
		elif rule == "candidateId":
			docs = collection.aggregate([{"$match":query},{"$group":{"_id":"$jobId","cId":{"$addToSet":"$candidateId"}}}])
		else:
			print query,"=============\n\n\n",Filter
			Filter["_id"] = False
			docs = collection.find(query,Filter)
		docList = [i for i in docs]
		return docList
		
	#def GetHistory(self,rule,params)
	def GetUserInfo(self,useremail,rule):
		rDict = dict()
		rDict["status"] = "Error"
		if rule == "admin":
			rDict["status"] = "success"
			return rDict
		dbh = mongoConnector.MongoConnector.getInstance()
                dbc = dbh.getDatabaseClient(self.dbName)
                if dbc is None:
                        print err.errDict["err_db_none"] % self.dbname
                        return
                collection = dbh.getCollection("usercollection")
                if collection is None:
                        print err.errDict["err_collection_none"] % 'usercollection'
                        return
		querydict = dict()
		querydict["email"] = useremail
		docs = collection.find(querydict,{"_id":False}).sort([("date",-1)]).limit(1)
		docList = [i for i in docs]
		if docList :
			rDict["userInfo"] = docList[0]
		else:
			rDict["userInfo"] = {}
		return rDict

	def GetJob(self):
		rDict = dict()
		rDict["status"] = "Error"
		dbh = mongoConnector.MongoConnector.getInstance()
                dbc = dbh.getDatabaseClient(self.dbName)
                if dbc is None:
                        print err.errDict["ERR_DB_NONE"] % self.dbName
                        return
                collection = dbh.getCollection("job")
                if collection is None:
                        print err.errDict["ERR_COLLECTION_NONE"] % 'job'
                        return
		queryDict = dict()
		rDict = collection.find(queryDict,{"_id":False}).sort([("date",-1)])
		return rDict
	def GetGoogleCelender(self):
        	SCOPES = ['https://www.googleapis.com/auth/calendar']
        	SERVICE_ACCOUNT_FILE = '/home/ubuntu/workspace/.private_keys/service.json'

        	credentials = service_account.Credentials.from_service_account_file(
                SERVICE_ACCOUNT_FILE, scopes=SCOPES)

        	service = googleapiclient.discovery.build('calendar', 'v3', credentials=credentials)
        	return service
	
	'''def GetData(self,collectionName, numOfRecord, pageNumber, _id):
        	rDict = dict()
                rDict["status"] = "Error"
		# connect db
        	try:
                	mg = mongoConnector.MongoConnector.getInstance()
                	dbc = mg.getDatabaseClient('antlegs')
        	except Exception as e:
                	rDict["ERROR:"] = "Database connection error"
                	return rDict

        	# get data
        	col = mg.getCollection(collectionName)
        	doc = col.find({},{'_id':_id}.sort({'_id':-1}).limit(numOfRecord*pageNumber))
        	allData = [i for i in doc]

        	# pagination
        	paginator = Paginator(allData, numOfRecord)
        	try:
                	page = paginator.page(pageNumber)
                	data = page.object_list
        	# If page is not an integer, deliver first page.
        	except PageNotAnInteger:
			page = paginator.page(1)
                	data = page.object_list
        	# If page is out of range (e.g. 9999), deliver last page of results.
        	except EmptyPage:
                	page = paginator.page(paginator.num_pages)
                	data = page.object_list

        	rDict[collectionName] = data
        	rDict["status"] = "Success"

        	return rDict
	def PostData(self,collectionName, serializer, record):
        	rDict = dict()
                rDict["status"] = "Error"
		# check datatype
        	serializer = serializer(data=record)
        	if serializer.is_valid():
                	# connect db
                	try:
                        	mg = mongoConnector.MongoConnector.getInstance()
                        	dbc = mg.getDatabaseClient('antlegs')
                	except Exception as e:
                        	rDict["ERROR:"] = "Database connection error"
                        	return rDict

                	# insert data
                	try:
                        	mg.insertCollection(collectionName, record)
                        	rDict["status"] = "Success"
                	except:
                        	rDict["ERROR"] = "DB insert error"
                        	return rDict
        	else:
                	rDict["ERROR"] = serializer.errors
			return rDict'''
	def GetCompany(self,rule):
		rDict = dict()
		rDict["status"] = "Error"
		dbh = mongoConnector.MongoConnector.getInstance()
                dbc = dbh.getDatabaseClient(self.dbName)
                if dbc is None:
                        print err.errDict["ERR_DB_NONE"] % self.dbName
                        return
                collection = dbh.getCollection("userCollection")
                if collection is None:
                        print err.errDict["ERR_COLLECTION_NONE"] % 'userCollection'
                        return
		queryDict = dict()
		queryDict["userType"] = rule["userType"]
		docs = collection.find(queryDict,{"_id":False})
		docList = [i for i in docs]
		
		if docList:
			docList.sort(key=lambda x:x["date"], reverse=True)
			
		rDict[rule["userType"]] = docList
		rDict["status"] = "success"
		return rDict
	def UpdateInfo(self,dDict,rule):
		rDict =dict() 
		rDict["status"] = "Error"
		dbh = mongoConnector.MongoConnector.getInstance()
                dbc = dbh.getDatabaseClient(self.dbName)
                if dbc is None:
                        print err.errDict["ERR_DB_NONE"] % self.dbName
                        return

		if rule == "activated":
			queryDict = dict()
			queryDict["userType"] = dDict["userType"] 
			queryDict["userId"] = dDict["userId"]
	                collection = dbh.getCollection("userCollection")
                	if collection is None:
                        	print err.errDict["ERR_COLLECTION_NONE"] % 'userCollection'
                        	return
			docs = collection.find(queryDict,{"_id":False}).sort([("date",-1)]).limit(1)
			docList = [i for i in docs]
			if docList:
				for i in docList:
					del i["regStatus"]
					tmpDict = i.copy()
					i["lastEdited"] = datetime.now()
					i["regStatus"] = rule
					dbh.updateCollection_dash(tmpDict,"userCollection",i)
					rDict["email"] = i["email"]
			else:
				rDict["error"] = "ERROR:Ther is no email nad usertype exist"
			rDict["status"] = "success"
			return rDict

	
