import os
import pandas as pd
from datetime import datetime
from antleg.dataconnector import mongoConnector
from antleg.errcode import errcode as err
import universities
class ExcelParser:
	def __init__(self):
		self.dbName = "antlegs"
		self._baseDir = '../../templates/'
		pass
	def ParseExcelFile(self,fileName,rule = 'dropdown'):
		rDict = {}
		self.fileName = fileName
		if rule == 'dropdown': 
			if not os.path.exists(self._baseDir + 'dropdownInfo/'):
				rDict["status"] = "ERROR"
				rDict["Error"] = "ERROR:There is no folder named dropdownInfo"
				return rDict
			_file = self._baseDir + 'dropdownInfo/'+self.fileName+'.xlsx' 
			if not os.path.isfile(_file):
				rDict["status"] = "ERROR"
				rDict["Error"] = "ERROR:There is no file .."
				return rDict
			rDict = self.ParseExcel(_file)
			return rDict
		return rDict
	def ParseExcel(self,_file):
		rDict = dict()
		rDict["status"] = "ERROR"
		sheets = []
		try:
			pE = pd.ExcelFile(_file)
		except:
			rDict['Error']  = 'ERROR:Excel parsing error'
			return rDict
		sheets = pE.sheet_names
		for i in sheets:
			rule = 'simple'
			reDict = self.ParseSheet(pE,i,'simple')
			if reDict["status"] == 'Error':
				rDict["Error"] = reDict["Error"]
				return rDict
		rDict["status"] = 'success'
		return rDict
		
	def ParseSheet(self,pE,sheetName,rule):
		if rule == 'simple':
			sheet = pE.parse(sheetName)
			if "NO" in sheet.columns:
				 sheet['NO'] = [i for i in xrange(len(sheet))]
			sheet.sort_values(sheetName)
			sheet.columns = map(str,sheet.columns)
			sheet.columns = map(str.lower,sheet.columns)
			sheet.columns = [i.replace(" ","") for i in sheet.columns]

			records = sheet.to_dict('records')
			reDict = self.UpdateInDb(sheetName,records,rule)
			return reDict
	def UpdateInDb(self,sheetName,record,rule): 
		rDict = dict()
		updateDict = dict()
		updateDict['type'] = sheetName.lower()
		updateDict["info"] = record
		updateDict["date"] = datetime.today()
		dbh = mongoConnector.MongoConnector.getInstance()
                dbc = dbh.getDatabaseClient(self.dbName)
                if dbc is None:
                        print err.errDict["ERR_DB_NONE"] % self.dbName
                        return 
                collection = dbh.getCollection("job")
		if collection is None:
                        print err.errDict["ERR_COLLECTION_NONE"] % 'job'
                        return
		dbh.updateCollection_dash({"date":datetime.today()},'dropInfoCollection' ,updateDict)
		rDict["status"] = 'success'
		return rDict 
	def GetDropInfo(self,Type):
		rDict = dict()
		rDict["status"] = "ERROR"
		if Type == "universities":
			uni = universities.API()
			all_data = uni.get_all()
			#all_data_list = [{"university":i.name,"country":i.country} for i in all_data if i.country in ["Canada","India","United States","Australia","United Arab Emirates","Switzerland","Qatar"]]
			all_data_list = [{"university":i.name,"country":i.country} for i in all_data if i.country in ["Canada","India","United States","Australia"]]
			rDict["uList"] = all_data_list
			rDict["status"] = 'success'
			return rDict
		dbh = mongoConnector.MongoConnector.getInstance()
                dbc = dbh.getDatabaseClient(self.dbName)
                if dbc is None:
                        print err.errDict["ERR_DB_NONE"] % self.dbName
                        return 
                collection = dbh.getCollection("dropInfoCollection")
		if collection is None:
                        print err.errDict["ERR_COLLECTION_NONE"] % 'job'
                        return
		docs = collection.find({'type':Type},{"_id":False}).sort([("date",-1)]).limit(1)
		docList = [i for i in docs]
		if not docList:
			rDict["Error"] = 'There is no information uploaded or please check type '
			return rDict
		rDict["status"] = 'success'
		rDict["info"] = docList[0]["info"]
		return rDict

